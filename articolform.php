<link rel="stylesheet" type="text/css" href="css/article_form_style.css" href="css/style.css">
<div class="container">
    <form action="addArticle.php" method="post">
        <div class="row">
            <div class="col-25">
                <label for="fname">First Name</label>
            </div>
            <div class="col-75">
                <input type="text" id="fname" name="firstname" placeholder="Your name..">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="lname">Last Name</label>
            </div>
            <div class="col-75">
                <input type="text" id="lname" name="lastname" placeholder="Your last name..">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="lname">Email Adress</label>
            </div>
            <div class="col-75">
                <input type="text" id="email" name="email" placeholder="Your email..">
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="country">Country</label>
            </div>
            <div class="col-75">
                <select id="country" name="country">
                    <option value="australia">Australia</option>
                    <option value="canada">Canada</option>
                    <option value="usa">USA</option>
                    <option value="romania">Romania</option>
                </select>
            </div>
        </div>
        <div class="row">
            <div class="col-25">
                <label for="subject">Article</label>
            </div>
            <div class="col-75">
                <textarea id="article" name="article_content" placeholder="Write your article..." style="height:200px"></textarea>
            </div>
        </div>
        <div class="row">
            <input type="submit" value="Post article">
        </div>
    </form>
</div>

</body>